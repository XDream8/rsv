mod daemon;
pub mod service;

use daemon::service_management_daemon;

// for cli-args
use seahorse::{App, Context, Flag, FlagType};
use std::env;
use std::process::exit;

fn main() {
    let args: Vec<String> = env::args().collect();

    let app: App = App::new(env!("CARGO_PKG_NAME"))
        .description(env!("CARGO_PKG_DESCRIPTION"))
        .version(env!("CARGO_PKG_VERSION"))
        .usage(format!("{} [service_dir(s)]", env!("CARGO_PKG_NAME")))
        .action(action)
        .flag(
            Flag::new("socket", FlagType::Bool)
                .description("set socket-path (defaults to /tmp/)")
                .alias("s"),
        );

    // Run the service manager
    app.run(args);
}

fn action(c: &Context) {
    if c.args.is_empty() {
        c.help();
        exit(0);
    } else {
        let socket_path: String = match c.string_flag("socket") {
            Ok(socket) => socket,
            _ => "/tmp/rsv_service_manager.sock".to_string(),
        };
        let mut directories: Vec<&str> = Vec::new();
        c.args.iter().for_each(|arg| directories.push(arg));

        // add default service directory, if directories is empty
        if directories.is_empty() {
            directories.push("/var/service")
        }

        for directory in directories {
            service_management_daemon(directory.to_string(), socket_path.as_str());
        }
    }
}
