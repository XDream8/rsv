use crate::service::Service;
use std::collections::HashSet;
use std::fs;
use std::path::PathBuf;

pub fn service_management_daemon(service_dir: String, socket_path: &str) {
    // This is where we will store running services
    let mut services: Vec<Service> = Vec::new();

    loop {
        // Scan "service_dir" directory for service directories
        let service_dirs = match fs::read_dir(service_dir.clone()) {
            Ok(dirs) => dirs,
            Err(err) => {
                eprintln!("Error reading service directory: {}", err);
                break;
            }
        };

        // Collect existing service directories
        let existing_service_dirs: HashSet<String> = services
            .iter()
            .map(|service| service.directory.clone())
            .collect();

        // Collect new service directories
        let new_service_dirs: Vec<PathBuf> = service_dirs
            .filter_map(|entry| {
                if let Ok(entry) = entry {
                    let path = entry.path();
                    if path.is_dir()
                        && path.file_name().unwrap().to_string_lossy() != "supervise"
                        && !existing_service_dirs.contains(&path.to_string_lossy().to_string())
                    {
                        Some(path)
                    } else {
                        None
                    }
                } else {
                    None
                }
            })
            .collect();

        // Start new services
        for service_dir in new_service_dirs {
            if let Some(service_name) = service_dir.file_name().and_then(|name| name.to_str()) {
                let mut service = Service::new(
                    service_name.to_string(),
                    service_dir.to_string_lossy().to_string(),
                );
                service.start();
                services.push(service);
            }
        }

        // Stop and remove stopped services
        let mut i = 0;
        while i < services.len() {
            if services[i].running && !services[i].is_running() {
                services.remove(i);
            } else {
                i += 1;
            }
        }

        // Sleep for some duration before checking again
        std::thread::sleep(std::time::Duration::from_secs(2));
    }
}
