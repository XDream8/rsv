use std::process::{Child, Command, ExitStatus, Stdio};

#[derive(Debug)]
pub struct Service {
    pub name: String,
    pub directory: String,
    pub process: Option<ChildWrapper>,
    pub running: bool,
}

#[derive(Debug)]
pub struct ChildWrapper {
    pub child: Option<Child>,
    pub exit_status: Option<ExitStatus>,
    pub pid: Option<u32>,
}

impl Service {
    pub fn new(name: String, directory: String) -> Service {
        Service {
            name,
            directory,
            process: None,
            running: false,
        }
    }

    pub fn start(&mut self) {
        if self.is_running() {
            println!("Service '{}' is already running.", self.name);
            return;
        }

        let mut cmd: Command = Command::new(format!("{}/run", self.directory));
        cmd.stdout(Stdio::null()).stderr(Stdio::null());

        match cmd.spawn() {
            Ok(child) => {
                let pid: u32 = child.id();
                let child_wrapper: ChildWrapper = ChildWrapper {
                    child: Some(child),
                    exit_status: None,
                    pid: Some(pid),
                };
                self.process = Some(child_wrapper);
                self.running = true;
                println!("Service '{}' started successfully.", self.name);
            }
            Err(err) => {
                eprintln!("Failed to start service '{}': {}", self.name, err);
            }
        }
    }

    pub fn stop(&mut self) {
        if let Some(process) = self.process.as_mut() {
            if let Some(mut child) = process.child.take() {
                if let Err(err) = child.kill() {
                    eprintln!("Failed to stop service '{}': {}", self.name, err);
                    return;
                }
            }
            self.process = None;
            self.running = false;
            println!("Service '{}' stopped.", self.name);
        } else {
            println!("Service '{}' is not running.", self.name);
        }
    }

    pub fn is_running(&mut self) -> bool {
        if let Some(process) = &mut self.process {
            if process.exit_status.is_none() {
                if let Some(child) = &mut process.child {
                    match child.try_wait() {
                        Ok(Some(status)) => {
                            process.exit_status = Some(status);
                            self.running = false;
                            println!(
                                "Service '{}' has terminated with exit code: {}",
                                self.name, status
                            );
                            false
                        }
                        Ok(None) => true,
                        Err(err) => {
                            self.running = false;
                            println!("Failed to check status of service '{}': {}", self.name, err);
                            false
                        }
                    }
                } else {
                    self.running = false;
                    false
                }
            } else {
                self.running = false;
                false
            }
        } else {
            self.running = false;
            false
        }
    }
}

pub fn start_service(services: &mut [Service], service_name: &str) {
    if let Some(service) = services.iter_mut().find(|s| s.name == service_name) {
        service.start();
    } else {
        println!("Service '{}' not found.", service_name);
    }
}

pub fn stop_service(services: &mut [Service], service_name: &str) {
    if let Some(service) = services.iter_mut().find(|s| s.name == service_name) {
        if service.is_running() {
            service.stop();
        } else {
            println!("Service '{}' is not running.", service_name);
        }
    } else {
        println!("Service '{}' not found.", service_name);
    }
}

pub fn restart_service(services: &mut [Service], service_name: &str) {
    if let Some(service) = services.iter_mut().find(|s| s.name == service_name) {
        service.stop();
        service.start();
    } else {
        println!("Service '{}' not found.", service_name);
    }
}

pub fn list_services(services: Vec<Service>) {
    for service in services {
        println!("Service: {}", service.name);
        println!("Directory: {}", service.directory);
        println!(
            "Pid: {}",
            service
                .process
                .unwrap_or(ChildWrapper {
                    child: None,
                    exit_status: None,
                    pid: Some(0000)
                })
                .pid
                .unwrap()
        );
        println!("Running: {}\n", service.running);
    }
}
