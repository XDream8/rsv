use seahorse::Context;

use crate::service::Service;
use crate::service::load_services;
use crate::service::{start_service, stop_service, restart_service, list_services};

pub fn start_services_action(c: &Context) {
    let mut services: Vec<&str> = Vec::new();
    c.args.iter().for_each(|arg| services.push(arg));

    let service_dir: String = match c.string_flag("service-dir") {
        Ok(dir) => dir,
        _ => "/var/service".to_string()
    };

    for service in services {
        start_service(service_dir.as_str(), service);
    }
}

pub fn stop_services_action(c: &Context) {
    let mut services: Vec<&str> = Vec::new();
    c.args.iter().for_each(|arg| services.push(arg));

    let service_dir: String = match c.string_flag("service-dir") {
        Ok(dir) => dir,
        _ => "/var/service".to_string()
    };

    for service in services {
        stop_service(service_dir.as_str(), service);
    }
}

pub fn restart_services_action(c: &Context) {
    let mut services: Vec<&str> = Vec::new();
    c.args.iter().for_each(|arg| services.push(arg));

    let service_dir: String = match c.string_flag("service-dir") {
        Ok(dir) => dir,
        _ => "/var/service".to_string()
    };

    for service in services {
        restart_service(service_dir.as_str(), service);
    }
}

pub fn list_services_action(c: &Context) {
    let mut services: Vec<&str> = Vec::new();
    c.args.iter().for_each(|arg| services.push(arg));

    let service_dir: String = match c.string_flag("service-dir") {
        Ok(dir) => dir,
        _ => "/var/service".to_string()
    };

    list_services(service_dir.as_str());
}
