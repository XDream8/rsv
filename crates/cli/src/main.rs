// for cli-args
use seahorse::{App, Command, Context, Flag, FlagType};
use std::env;
use std::process::exit;

// communicate with daemon through socket
use std::io::{Read, Write};
use std::os::unix::net::UnixStream;

fn main() {
    let args: Vec<String> = env::args().collect();

    let app = App::new(env!("CARGO_PKG_NAME"))
        .description(env!("CARGO_PKG_DESCRIPTION"))
        .version(env!("CARGO_PKG_VERSION"))
        .usage(format!("{} [flags]", env!("CARGO_PKG_NAME")))
        .action(action)
        .command(
            Command::new("start")
                .description("Start a service")
                .flag(service_dir_flag())
                .flag(socket_flag())
                .action(start_action),
        )
        .command(
            Command::new("stop")
                .description("Stop a service")
                .flag(service_dir_flag())
                .flag(socket_flag())
                .action(stop_action),
        )
        .command(
            Command::new("restart")
                .description("restart service(s)")
                .usage("rsv restart <services>")
                .flag(service_dir_flag())
                .flag(socket_flag())
                .action(restart_action),
        )
        .command(
            Command::new("services")
                .description("prints a list of service(s)")
                .usage("rsv services")
                .alias("list")
                .flag(service_dir_flag())
                .flag(socket_flag())
                .action(list_action),
        );

    // Run the service manager
    app.run(args);
}

fn service_dir_flag() -> Flag {
    Flag::new("service-dir", FlagType::String)
        .description("Custom service directory")
        .alias("s")
}
fn socket_flag() -> Flag {
    Flag::new("socket", FlagType::String)
        .description("Unix domain socket path")
        .alias("sock")
}

fn start_action(c: &seahorse::Context) {
    let socket_path: String = c
        .string_flag("socket")
        .unwrap_or_else(|_| "/tmp/rsv_service_manager.sock".to_string());

    let services: Vec<&str> = c.args.iter().map(|l| l.as_str()).collect();

    send_command("start", &socket_path, &services);
}

fn stop_action(c: &seahorse::Context) {
    let socket_path: String = c
        .string_flag("socket")
        .unwrap_or_else(|_| "/tmp/rsv_service_manager.sock".to_string());

    let services: Vec<&str> = c.args.iter().map(|l| l.as_str()).collect();

    send_command("stop", &socket_path, &services);
}

fn restart_action(c: &seahorse::Context) {
    let socket_path: String = c
        .string_flag("socket")
        .unwrap_or_else(|_| "/tmp/rsv_service_manager.sock".to_string());

    let services: Vec<&str> = c.args.iter().map(|l| l.as_str()).collect();

    send_command("restart", &socket_path, &services);
}

fn list_action(c: &seahorse::Context) {
    let socket_path: String = c
        .string_flag("socket")
        .unwrap_or_else(|_| "/tmp/rsv_service_manager.sock".to_string());

    send_command("list", &socket_path, &Vec::<&str>::new());
}

fn send_command(command: &str, socket_path: &str, services: &[&str]) {
    let mut stream = match UnixStream::connect(socket_path) {
        Ok(stream) => stream,
        Err(err) => {
            eprintln!("Failed to connect to daemon: {}", err);
            return;
        }
    };

    let services_data = services.join(",");

    if let Err(err) = stream.set_read_timeout(Some(std::time::Duration::from_secs(5))) {
        eprintln!("Failed to set socket read timeout: {}", err);
        return;
    }

    if let Err(err) = stream.set_write_timeout(Some(std::time::Duration::from_secs(5))) {
        eprintln!("Failed to set socket write timeout: {}", err);
        return;
    }

    if let Err(err) = stream.write_all(command.as_bytes()) {
        eprintln!("Failed to send command: {}", err);
        return;
    }

    if let Err(err) = stream.write_all(services_data.as_bytes()) {
        eprintln!("Failed to send service names: {}", err);
        return;
    }

    let mut response = String::new();
    match stream.read_to_string(&mut response) {
        Ok(_) => println!("Response: {}", response),
        Err(err) => {
            eprintln!("Failed to read response: {}", err);
        }
    }
}

fn action(c: &Context) {
    if c.args.is_empty() {
        c.help();
        exit(0);
    }
}
