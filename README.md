<div align="center">
    <h1>rsv</h1>
    <p>a service manager similar to runit written in rust</p>
</div>

## idea
a service manager's only purpose should be managing processes. this means no `supervise` directory at all.

## whats done

- [X] basic service management
- [X] dynamic server starting/restarting
- [ ] a cli tool to manage services

## building from git source
```sh
$ git clone https://codeberg.org/XDream8/rsv
$ cd rsv
$ cargo build --profile optimized
$ ./target/optimized/rsvd
```
